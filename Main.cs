﻿using BioLab.ImageProcessing;

using BioLab.Biometrics;
using BioLab.DimensionalityReduction;
using BioLab.Math.LinearAlgebra;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BioLab.Common;

namespace SmartphoneFingerprintClassification
{
    public partial class Main : Form
    {
        RgbImage<byte> inputImage;
        public Main()
        {
            InitializeComponent();
        }

        private void FileListControl1_SelectedFileNameChanged(object sender, EventArgs e)
        {
            inputImage = ImageBase.LoadFromFile(fileListControl1.SelectedFileName).ToByteRgbImage();
            imageViewerInputImage.Image = inputImage;
            ProcessImage();
          
        }

        private void ProcessImage()

        {

            var normalization = new NormalizationStep(imageViewerInputImage.Image.ToByteRgbImage());
            var normalizationResult = normalization.Execute();
            imageViewerNormalizedImage.Image = normalizationResult;

            var segmentation = new SegmentationStep(normalizationResult);
            var segmentationResult = segmentation.Execute();
            imageViewerSegmentedImage.Image = segmentationResult;

            var rectification = new RectificationStep(segmentationResult, segmentation.RoiFingerContour, segmentation.RoiFingerBinMask);
            var rectificationResult = rectification.Execute();
            imageViewerRectifiedImage.Image = rectificationResult;

            var orientation = new OrientationStep(rectificationResult, rectification.ElementMask);
            var orientationResult = orientation.Execute();
            orientationImageViewer.OrientationImage = orientationResult;



        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
    
}
