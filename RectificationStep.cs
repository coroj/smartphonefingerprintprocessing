﻿using BioLab.Biometrics.Fingerprint;
using BioLab.ImageProcessing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartphoneFingerprintClassification
{
    class RectificationStep : ImageOperation<Image<byte>, Image<byte>>
    {
        private Image<byte> roiFingerImage;
        public VectorOfPoint RoiFingerContour { get; private set; }
        public Image<Gray, byte> RoiFingerBinMask { get; private set; }

        public Image<byte> RoiFingerTipImage { get; private set; }
        public Image<byte> RoiFingerTipMeanImage { get; private set; }
        public Image<byte> RoiFingerTipDenoisedImage { get; private set; }
        public Image<Gray,byte> PixelDistributionOnFinger { get; private set; }

        public VectorOfPoint RoiFingerTipContour { get; private set; }
        public Image<Gray, byte> RoiFingerTipBinMask { get; private set; }

        public OrientationImage OrientationImage { get; private set; }

        public Image<byte> ElementMask { get; private set; }
        public FrequencyImage FrequencyImage { get; private set; }
        

        public RectificationStep(Image<byte> input,VectorOfPoint contour,Image<Gray,byte> binMask)
        {
            roiFingerImage = input.Clone();
            InputImage = input.Clone();
            RoiFingerContour = contour;
            RoiFingerBinMask = binMask;
        }
        private void fingerTipExtraction()
        {
            PixelDistributionOnFinger = Utils.fingerPixelDistribuition(RoiFingerBinMask);
            PixelDistributionOnFinger.Save("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\distribution.jpg");
            var justOkPixel = Utils.pixelDistributionBasedTipImage(PixelDistributionOnFinger);

            var boundingRect = CvInvoke.BoundingRectangle(Utils.findBestExternalContour(justOkPixel));

            RoiFingerTipImage = Utils.extractROI(roiFingerImage, boundingRect);
          
            RoiFingerTipContour = Utils.findBestExternalContour(Utils.fromBiolabGrayToOpenCVGrayForROI(RoiFingerTipImage));

            RoiFingerTipBinMask = new Image<Gray, byte>(RoiFingerTipImage.Width, RoiFingerTipImage.Height);
            Utils.fillContourImage(RoiFingerTipBinMask, RoiFingerTipContour);

            RoiFingerTipBinMask.Save("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\roifingertipbinmask.jpg");

        }
        private void enhanceAndExtractGabor() {
            /* orientazione dito
            * var vectorSet = new BioLab.Common.FeatureVectorSet();
           for (var i = 0; i < roiFingerContour.Size; i++)
           {

               var vector = new BioLab.Common.FeatureVector(2);
               vector[0] = roiFingerContour[i].X;
               vector[1] = roiFingerContour[i].Y;
               vectorSet.Add(vector, true);

           }

           var pca = (PcaTransform)(new PcaTransformBuilder(vectorSet, 2).Calculate());
           var bases = pca.Bases;
           var eigenvalues = pca.Eigenvalues;
           Point center = new Point((int)Math.Round(pca.Origin[0]), (int)Math.Round(pca.Origin[1]));
           CvInvoke.Circle(roiFingerBinMask, center ,4,new MCvScalar(0,0,0),2);
           Console.WriteLine("eigenvalues:"+eigenvalues);
           Point p1 = new Point((int)(Math.Round(center.X + (0.02 * bases[0, 0] * eigenvalues[0]))),(int) Math.Round((center.Y + (0.02 * bases[0, 1] * eigenvalues[0]))));
           CvInvoke.Line(roiFingerBinMask, center, p1, new MCvScalar(0, 0, 0));
           Point p2 = new Point((int)(Math.Round(center.X + (0.02 * bases[1,0] * eigenvalues[1]))), (int)Math.Round((center.Y + (0.02 * bases[1, 1] * eigenvalues[1]))));
           //CvInvoke.Line(roiFingerBinMask, center, p2, new MCvScalar(0, 0, 0));
           //Point p1 = center + 0.02 * Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
           //circle(img, cntr, 3, Scalar(255, 0, 255), 2);

           //Point p1 = cntr + 0.02 * Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
           //Point p2 = cntr - 0.02 * Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
           //drawAxis(img, cntr, p1, Scalar(0, 255, 0), 1);
           //drawAxis(img, cntr, p2, Scalar(255, 255, 0), 5);

           Console.WriteLine(bases);
           double radAngle = Math.Atan2(bases[0, 0], bases[0, 1]);


           double angle = (radAngle * (180.0 / Math.PI));*/

            // MessageBox.Show(angle.ToString());

            var rectificationFilter = Utils.createSmoothingFilter(3);

            RoiFingerTipMeanImage = new ByteToByteConvolution(RoiFingerTipImage, rectificationFilter, 0).Execute();

            RoiFingerTipDenoisedImage = RoiFingerTipImage.Clone();

            for (var i = 0; i < RoiFingerTipDenoisedImage.PixelCount; i++)
            {
                if (RoiFingerTipDenoisedImage[i] > Utils.toHide)
                {
                    RoiFingerTipDenoisedImage[i] = (RoiFingerTipDenoisedImage[i] - RoiFingerTipMeanImage[i] + 150).ClipToByte();
                }
            }


            ImageBlockIterator imageBlockIterator = new ImageBlockIterator((ImageBase)RoiFingerTipDenoisedImage, Utils.size, Utils.size, Utils.step, Utils.step, Utils.border, Utils.border);
            ElementMask = new Image<byte>(imageBlockIterator.BlocksPerRow, imageBlockIterator.BlocksPerColumn, byte.MaxValue);

            var orientationOperation = new SimpleGradientOrientationExtraction(RoiFingerTipDenoisedImage, Utils.step, Utils.size, Utils.border, ElementMask);

            OrientationImage = orientationOperation.Execute();

            var frequencyOperation = new FrequencyImageExtractionXSignature(RoiFingerTipDenoisedImage, OrientationImage, ElementMask);
            frequencyOperation.PixelwiseMask = Utils.fromOpenCVGrayToBiolabGrayForROI(RoiFingerTipBinMask);


            FrequencyImage = frequencyOperation.Execute();


            var gaborImage = new FingerprintEnhancementGabor(RoiFingerTipDenoisedImage, OrientationImage, FrequencyImage).Execute();

            Result = gaborImage;
        }

        public override void Run()
        {

            fingerTipExtraction();
            enhanceAndExtractGabor();
           
        }
    }
}
