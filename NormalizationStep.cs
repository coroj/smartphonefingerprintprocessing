﻿using System;
using System.Drawing;
using BioLab.ImageProcessing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace SmartphoneFingerprintClassification
{
    class NormalizationStep : ImageOperation<RgbImage<byte>,RgbImage<byte>>
    {
        private RgbImage<byte> image;
        public NormalizationStep(RgbImage<byte> input)
        {
            InputImage = new RgbImage<byte>(input);
            image = new RgbImage<byte>(input);
        }
        public override void Run()
        {
            var rgbInputImage = image;


            var cvRgbInputImage = Utils.fromBiolabRGBToOpenCVRGB(rgbInputImage);

            var detailed = new Image<Rgb, byte>(rgbInputImage.Width, rgbInputImage.Height);

            var CVYCbCrInputImage = new Image<Ycc, byte>(rgbInputImage.Width, rgbInputImage.Height);
            CvInvoke.CvtColor(cvRgbInputImage, CVYCbCrInputImage, ColorConversion.Rgb2YCrCb);

            var YComponent = CVYCbCrInputImage[0];
            var newYComponent = new Image<Gray, byte>(YComponent.Width, YComponent.Height);

            CvInvoke.CLAHE(YComponent, 3, new Size(30, 30), newYComponent);
            CVYCbCrInputImage[0] = newYComponent;

            CvInvoke.CvtColor(CVYCbCrInputImage, cvRgbInputImage, ColorConversion.YCrCb2Rgb);

            CvInvoke.DetailEnhance(cvRgbInputImage, detailed);

            Result = Utils.fromOpenCVRGBToBiolabRGB(detailed);
        }
    }
}
