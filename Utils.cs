﻿using BioLab.ImageProcessing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartphoneFingerprintClassification
{
    class Utils
    {
        public static byte toHide = 0;
        public static byte toShow = 255;
        public static int step = 30;
        public static int size = 61;
        public static int border = 0;
        public static Image<Rgb, byte> fromBiolabRGBToOpenCVRGB(RgbImage<byte> bImage)
        {
            var CVRChannel = new Emgu.CV.Image<Gray, byte>(bImage.Width, bImage.Height);
            CVRChannel.Bytes = bImage.RedChannel.CopyPixels();

            var CVGChannel = new Emgu.CV.Image<Gray, byte>(bImage.Width, bImage.Height);
            CVGChannel.Bytes = bImage.GreenChannel.CopyPixels();

            var CVBChannel = new Emgu.CV.Image<Gray, byte>(bImage.Width, bImage.Height);
            CVBChannel.Bytes = bImage.BlueChannel.CopyPixels();

            var cvRgbImage = new Emgu.CV.Image<Rgb, byte>(bImage.Width, bImage.Height);
            cvRgbImage[0] = CVRChannel;
            cvRgbImage[1] = CVGChannel;
            cvRgbImage[2] = CVBChannel;

            return cvRgbImage;
        }
        public static RgbImage<byte> fromOpenCVRGBToBiolabRGB(Emgu.CV.Image<Rgb, byte> oImage)
        {
            var RChannel = new Image<byte>(oImage.Width, oImage.Height, oImage[0].Bytes);
            var GChannel = new Image<byte>(oImage.Width, oImage.Height, oImage[1].Bytes);
            var BChannel = new Image<byte>(oImage.Width, oImage.Height, oImage[2].Bytes);

            return new RgbImage<byte>(RChannel, GChannel, BChannel);
        }
        public static Emgu.CV.Image<Gray, byte> fromBiolabGrayToOpenCVGray(Image<byte> bImage)
        {
            var imm = new Emgu.CV.Image<Gray, byte>(bImage.Width, bImage.Height);
            imm.Bytes = bImage.CopyPixels();
            return imm;
        }
        public static Image<byte> fromOpenCVGrayToBiolabGray(Emgu.CV.Image<Gray, byte> oImage)
        {
            return new Image<byte>(oImage.Width, oImage.Height, oImage.Bytes);
        }
        public static Image<byte> fromOpenCVGrayToBiolabGrayForROI(Emgu.CV.Image<Gray, byte> oImage)
        {
            var imm = new Image<byte>(oImage.Width, oImage.Height);
            for (var x = 0; x < oImage.Width; x++)
            {
                for (var y = 0; y < oImage.Height; y++)
                {
                    imm[y, x] = oImage[y, x].Intensity.RoundAndClipToByte();
                }
            }

            return imm;
        }
        public static Emgu.CV.Image<Gray, byte> fromBiolabGrayToOpenCVGrayForROI(Image<byte> bImage)
        {
            var imm = new Image<Gray, byte>(bImage.Width, bImage.Height);
            for (var x = 0; x < bImage.Width; x++)
            {
                for (var y = 0; y < bImage.Height; y++)
                {
                    imm[y, x] = new Gray(bImage[y, x]);
                }
            }

            return imm;
        }
        public static VectorOfPoint findBestExternalContour(Image<Gray, Byte> image)
        {
            var contours = new VectorOfVectorOfPoint();
            var hier = new Mat();

            CvInvoke.FindContours(image, contours, hier, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            var dictionary = new Dictionary<int, double>();
            for (var i = 0; i < contours.Size; i++)
            {
                dictionary.Add(i, CvInvoke.ContourArea(contours[i]));
            }

            var items = dictionary.OrderByDescending(v => v.Value);
            var key = items.First().Key;
            VectorOfPoint bestContour = contours[key];
            return bestContour;
        }
        public static void fillContourImage(Image<Gray, byte> image, VectorOfPoint contour)
        {
            var aVect = new VectorOfVectorOfPoint();
            aVect.Push(contour);
            //CvInvoke.DrawContours(updatedCvBinMask, aVect, 0, new MCvScalar(255, 255, 255));
            CvInvoke.FillPoly(image, aVect, new MCvScalar(255, 255, 255));
        }
        public static Image<Gray, byte> fingerPixelDistribuition(Image<Gray, byte> binaryImage)
        {
            var result = binaryImage.Clone();
            double[] pixelCount = new double[binaryImage.Height];
            for (int x = 0; x < binaryImage.Height; x++)
            {
                for (int y = 0; y < binaryImage.Width; y++)
                {
                    if (binaryImage[x, y].Intensity > 0)
                    {
                        pixelCount[x]++;
                    }
                }
                if(x >= 0 && x <= binaryImage.Height * 0.46)
                {
                    pixelCount[x] *= 0.45;
                }
              


            }

            for (int x = 0; x < binaryImage.Height; x++)
            {
                for (int y = 0; y < binaryImage.Width; y++)
                {
                    var oldVal = binaryImage[x, y].Intensity;
                    result[x, y] = new Gray(Math.Round((oldVal * 255) / (pixelCount[x])));
                }
            }

            return result;
        }

        public static Image<Gray,byte> pixelDistributionBasedTipImage(Image<Gray,byte> PDImage)
        {
            var imm = PDImage.Clone();
            for (int x = 0; x < PDImage.Height; x++)
            {
                for (int y = 0; y < PDImage.Width; y++)
                {
                    if(PDImage[x,y].Intensity < 240)
                    {
                        imm[x, y] = new Gray(0);
                    }
                }
            }

            return imm;
        }

        public static Image<byte> extractROI(Image<byte> image, Rectangle rect)
        {
            var imm = Utils.fromBiolabGrayToOpenCVGrayForROI(image);//se l'immagine passata è un roi o no funziona comunque

            var immBounded = imm.Copy(rect);

            return  Utils.fromOpenCVGrayToBiolabGrayForROI(immBounded);
        }
        public static ConvolutionFilter<int> createSmoothingFilter(int n)
        {
            var filter = new ConvolutionFilter<int>(n * n, n * n);
            for (var i = 0; i < n * n; i++)
            {
                filter[i] = 1;
            }
            return filter;
        }



    }
}

        



