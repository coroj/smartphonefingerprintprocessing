﻿using BioLab.Biometrics.Fingerprint;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartphoneFingerprintClassification
{
    class OrientationStep : ImageOperation<Image<byte>, OrientationImage>
    {
        public Image<byte> ElementMask {get; private set;} 
        public OrientationStep(Image<byte> image,Image<byte> elementMask)
        {
            this.InputImage = image;
            this.ElementMask = elementMask;
        }
 
        public override void Run()
        {
            Result = new SimpleGradientOrientationExtraction(InputImage, Utils.step, Utils.size, Utils.border, ElementMask).Execute();
        }
    }
}
