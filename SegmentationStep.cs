﻿using BioLab.ImageProcessing;
using System;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace SmartphoneFingerprintClassification
{
    class SegmentationStep : ImageOperation<RgbImage<byte>, Image<byte>>
    {


        private RgbImage<byte> image;
        public VectorOfPoint RoiFingerContour { get; private set; }
        public Image<Gray, byte> RoiFingerBinMask { get; private set; }
        public SegmentationStep(RgbImage<byte> input)
        {
            this.InputImage = new RgbImage<byte>(input);
            this.image = new RgbImage<byte>(input);

        }

        public override void Run()
        {

            var YCbCrImage = image.ToByteYCbCrImage();

            var cursor = new ImageCursor(YCbCrImage);
            cursor.Restart();
            var binMask = new Image<byte>(YCbCrImage.Width, YCbCrImage.Height, Utils.toHide);
            do
            {
                var rdiff = YCbCrImage.RedDifferenceChromaChannel[cursor];
                var bdiff = YCbCrImage.BluDifferenceChromaChannel[cursor];

                if (rdiff >= 144 && rdiff <= 173)
                {
                    if (bdiff >= 77 && bdiff <= 127)
                    //if (bdiff >= 85 && bdiff <= 150)
                    {
                        binMask[cursor] = Utils.toShow;
                    }
                }

            } while (cursor.MoveNext());

            binMask = new BioLab.ImageProcessing.Topology.MorphologyOpening(binMask, BioLab.ImageProcessing.Topology.MorphologyStructuringElement.CreateCircle(6), 255).Execute();

            var filter = Utils.createSmoothingFilter(3);
            binMask = new ByteToByteConvolution(binMask, filter, 0).Execute();

           
            binMask.SaveToFile("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\closedsmoothedbutdirtybinmask.jpg");


            var cvBinMask = Utils.fromBiolabGrayToOpenCVGray(binMask);
            var updatedCvBinMask = new Image<Gray, byte>(cvBinMask.Width, cvBinMask.Height, new Gray(0));

            VectorOfPoint bestContour = Utils.findBestExternalContour(cvBinMask);

            var boundingRect = CvInvoke.BoundingRectangle(bestContour);


            /*int sz = bestContour.Size;

            Mat dataPts = new Mat(new Size(sz, 2), Emgu.CV.CvEnum.DepthType.Cv64F, 1);

            double[] dataPtsData = new double[(int)(dataPts.Total.ToInt32() * dataPts.NumberOfChannels)];
            for (int i = 0; i < dataPts.Rows; i++)
            {
                dataPtsData[i * dataPts.Cols] = bestContour[i].X;
                dataPtsData[i * dataPts.Cols + 1] = bestContour[i].Y;
            }
            dataPts.SetTo(dataPtsData);

            // Perform PCA analysis
            Mat mean = new Mat();
            Mat eigenvectors = new Mat();

            CvInvoke.PCACompute(dataPts, mean, eigenvectors);
            double[] meanData = new double[(int)(mean.Total.ToInt32() * mean.NumberOfChannels)];
            mean.CopyTo(meanData);


            Point cntr = new Point((int)meanData[0], (int)meanData[1]);





            Matrix<double> matrix = new Matrix<double>(eigenvectors.Rows, eigenvectors.Cols);
            eigenvectors.CopyTo(matrix);*/


            /*double[] eigenvectorsData = new double[(eigenvectors.Total.ToInt32() * eigenvectors.NumberOfChannels)];

            eigenvectors.CopyTo(eigenvectorsData);*/


            /* double radAngle = Math.Atan2(matrix[0, 1], matrix[0, 0]); // orientation in radians


             double angle = radAngle * (180.0 / Math.PI);

             MessageBox.Show(angle.ToString());*/


            Utils.fillContourImage(updatedCvBinMask, bestContour);
            /* var hullVect = new VectorOfPoint();
             CvInvoke.ConvexHull(bestContour, hullVect);
             Point center = new Point(hullVect[hullVect.Size / 2].X, hullVect[hullVect.Size / 2].Y);
             CvInvoke.Circle(updatedCvBinMask, center , 20, new MCvScalar(255, 0, 0), 3);*/
            updatedCvBinMask.Save("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\provacentro.jpg");


            binMask = Utils.fromOpenCVGrayToBiolabGray(updatedCvBinMask);


            /*var rect = CvInvoke.MinAreaRect(bestContour);
            var angle = rect.Angle;

            if (rect.Size.Width < rect.Size.Height)
            {
                angle += 180;
            }
            else
            {
                angle += 90;
            }*/

            var grayImage = image.ToByteImage();
            cursor = new ImageCursor(grayImage);
            cursor.Restart();
            do
            {
                if (binMask[cursor] == Utils.toHide)
                {
                    grayImage[cursor] = Utils.toHide;
                }


            } while (cursor.MoveNext());


            var roiFingerImage = Utils.extractROI(grayImage, boundingRect);

            roiFingerImage = ImageUtilities.Resize(roiFingerImage, 600, 1200);


            RoiFingerContour = Utils.findBestExternalContour(Utils.fromBiolabGrayToOpenCVGrayForROI(roiFingerImage));


            //roiFingerBinMask.Save("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\roifingerbinmask.jpg");

            /*var vectorSet = new FeatureVectorSet();
            for (var i = 0; i < roiFingerContour.Size; i++)
            {

                var vector = new FeatureVector(2);
                vector[0] = roiFingerContour[i].X;
                vector[1] = roiFingerContour[i].Y;
                vectorSet.Add(vector, true);

            }

            var pca = (PcaTransform)(new PcaTransformBuilder(vectorSet, 2).Calculate());
            var bases = pca.Bases;
            var eigenvalues = pca.Eigenvalues;
            Point center = new Point((int)Math.Round(pca.Origin[0]), (int)Math.Round(pca.Origin[1]));
            Point extreme = new Point((int)Math.Round(bases[0, 0]), (int)Math.Round(bases[0, 1]));*/
            /*var hullVect = new VectorOfPoint();
            CvInvoke.ConvexHull(roiFingerContour, hullVect);
            Point extreme = new Point(hullVect[(hullVect.Size / 2)+10].X, hullVect[(hullVect.Size / 2)+10].Y);
            Point center = new Point(roiFingerImage.Width / 2, roiFingerImage.Height / 2);
            double radAngle = Math.Atan2(extreme.Y - center.Y, extreme.X - center.X);


            double angle = (radAngle * (180.0 / Math.PI));

            MessageBox.Show(angle.ToString());
            double ra = -100 - angle;
            roiFingerImage = new ByteAffineTransform(roiFingerImage, 0, 0, ra, 0).Execute();

            roiFingerContour = Utils.findBestExternalContour(Utils.fromBiolabGrayToOpenCVGrayForROI(roiFingerImage));*/
            RoiFingerBinMask = new Image<Gray, byte>(roiFingerImage.Width, roiFingerImage.Height);
            Utils.fillContourImage(RoiFingerBinMask, RoiFingerContour);

            RoiFingerBinMask.Save("C:\\Users\\utente\\Desktop\\SmartphoneFingerprintClassification\\roifingerbinmask.jpg");


            Result = roiFingerImage;
    



        }
    }
}


