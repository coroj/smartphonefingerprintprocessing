﻿namespace SmartphoneFingerprintClassification
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.fileListControl1 = new BioLab.GUI.UserControls.FileListControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.imageViewerInputImage = new BioLab.GUI.DataViewers.ImageViewer();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.imageViewerNormalizedImage = new BioLab.GUI.DataViewers.ImageViewer();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.imageViewerSegmentedImage = new BioLab.GUI.DataViewers.ImageViewer();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.imageViewerRectifiedImage = new BioLab.GUI.DataViewers.ImageViewer();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.orientationImageViewer = new BioLab.Biometrics.GUI.OrientationImageViewer();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.Controls.Add(this.fileListControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1028, 609);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // fileListControl1
            // 
            this.fileListControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileListControl1.FileFilter = "*.jpeg;*.jpg;*.png";
            this.fileListControl1.Location = new System.Drawing.Point(3, 3);
            this.fileListControl1.MinimumSize = new System.Drawing.Size(130, 100);
            this.fileListControl1.Name = "fileListControl1";
            this.fileListControl1.Size = new System.Drawing.Size(199, 603);
            this.fileListControl1.TabIndex = 0;
            this.fileListControl1.SelectedFileNameChanged += new System.EventHandler(this.FileListControl1_SelectedFileNameChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(207, 2);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(819, 605);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.imageViewerInputImage);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(811, 579);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input Image";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // imageViewerInputImage
            // 
            this.imageViewerInputImage.BackColor = System.Drawing.SystemColors.Control;
            this.imageViewerInputImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageViewerInputImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerInputImage.Location = new System.Drawing.Point(2, 2);
            this.imageViewerInputImage.Name = "imageViewerInputImage";
            this.imageViewerInputImage.Size = new System.Drawing.Size(807, 575);
            this.imageViewerInputImage.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.imageViewerNormalizedImage);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage5.Size = new System.Drawing.Size(811, 579);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Normalization";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // imageViewerNormalizedImage
            // 
            this.imageViewerNormalizedImage.BackColor = System.Drawing.SystemColors.Control;
            this.imageViewerNormalizedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageViewerNormalizedImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerNormalizedImage.Location = new System.Drawing.Point(2, 2);
            this.imageViewerNormalizedImage.Name = "imageViewerNormalizedImage";
            this.imageViewerNormalizedImage.Size = new System.Drawing.Size(807, 575);
            this.imageViewerNormalizedImage.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.imageViewerSegmentedImage);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(811, 579);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Segmentation";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // imageViewerSegmentedImage
            // 
            this.imageViewerSegmentedImage.BackColor = System.Drawing.SystemColors.Control;
            this.imageViewerSegmentedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageViewerSegmentedImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerSegmentedImage.Location = new System.Drawing.Point(2, 2);
            this.imageViewerSegmentedImage.Name = "imageViewerSegmentedImage";
            this.imageViewerSegmentedImage.Size = new System.Drawing.Size(807, 575);
            this.imageViewerSegmentedImage.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.imageViewerRectifiedImage);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(811, 579);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Rectification";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // imageViewerRectifiedImage
            // 
            this.imageViewerRectifiedImage.BackColor = System.Drawing.SystemColors.Control;
            this.imageViewerRectifiedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageViewerRectifiedImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerRectifiedImage.Location = new System.Drawing.Point(2, 2);
            this.imageViewerRectifiedImage.Name = "imageViewerRectifiedImage";
            this.imageViewerRectifiedImage.Size = new System.Drawing.Size(807, 575);
            this.imageViewerRectifiedImage.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.orientationImageViewer);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(811, 579);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Orientation Image";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // orientationImageViewer
            // 
            this.orientationImageViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orientationImageViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orientationImageViewer.FingerprintImage = null;
            this.orientationImageViewer.LineColor = System.Drawing.Color.Blue;
            this.orientationImageViewer.Location = new System.Drawing.Point(2, 2);
            this.orientationImageViewer.Margin = new System.Windows.Forms.Padding(4);
            this.orientationImageViewer.Name = "orientationImageViewer";
            this.orientationImageViewer.OrientationImage = null;
            this.orientationImageViewer.Size = new System.Drawing.Size(807, 575);
            this.orientationImageViewer.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 609);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Main Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private BioLab.GUI.UserControls.FileListControl fileListControl1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerInputImage;
        private System.Windows.Forms.TabPage tabPage5;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerNormalizedImage;
        private System.Windows.Forms.TabPage tabPage2;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerSegmentedImage;
        private System.Windows.Forms.TabPage tabPage3;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerRectifiedImage;
        private System.Windows.Forms.TabPage tabPage4;
        private BioLab.Biometrics.GUI.OrientationImageViewer orientationImageViewer;
    }
}

